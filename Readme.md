# Sound Play

## Intro

Reads .asm formatted sequences used for SM64 (and potentially other games?) and 
allows exporting, editing and creation of new SFX or effects.

## Required other tools
* armips (https://github.com/Kingcom/armips)
* PC port external loader (https://github.com/jesusyoshi54/sm64ex-alo/tree/sfx_test)
* Seq64 2.0 or greater (https://github.com/sauraen/seq64/)

## Usage

Use sp.py to load a sequence, and internal methods to output or edit the sequence (more to come in future).\

Use PC port player to load external SFX with name "dyncall.bin", or external m64 with name "test.m64".\

Use Seq64 2.0+ with included .XML file to I/O from binary .com/.m64 format to .mus/.asm format.