---------------------------------------------------------------------------
[NOTE TYPES
#note0 = (pitch, play percentage, velocity, duration)
#note1 = (pitch, play percentage, velocity)
#note2 = (pitch, velocity)

#float timeInSeconds = value2 * 60 / (BPM * 48);
]
---------------------------------------------------------------------------
[ASDR AND ENVELOPE VALUES

Not supported by seq64 or midi natively

envelopes
"stop" == [0,0] = stop note
"hang" == [-1,0] = sutain forever
"restart" == [-3,0] = go back to start of env with vol 0
"goto" == -2 = go to byte pair represented by num after this
otherwise, the pair represents time, volume
ex. [2, 32700]
volume is a percent rep by half / 32767.0f. 0x7FFF is the max volume [you can go over 1 but it isnt rec]
time is in audio thread updates? which is always 240 times a second.

Release
release rate is put in the inst dec in the JSON
it is a byte.
release is done when note is no longer pressed. After that, volume fades to 0, and note is disabled at 0.
To convert to time, use this formula:
release_rate * 0.0005859375*240
240 updates per second, with a scaling of 0.0005859375 volume per update per release rate.
Volume is a float, max is 1.0f

All volume while the note is pressed is controlled by the envelope.
When the note is released, it holds the volume for length determined by "sustain" and thendecays to nothing over a time of "release rate"\
]
---------------------------------------------------------------------------
[headset effects:

NO midi mapping

idk it just sounds different?
]
---------------------------------------------------------------------------
[PORTAMENTOS
cmd:
layer_portamento mode (u8), pitch target (u8), time (u16/u8)

MODES:
0x80 time is a u8 instead of a half, same time scaling is applied

mode 0 - no bend, disabled
mode 1 - start at target note, do portamento once
mode 2 - start at played note, do portamento once

mode 3 - start at target note, repeat portamento each note
mode 4 - start at played note, repeat portamento each note

mode 5 - start at target note, holds target note pitch, next notes become new target pitches

Time is in same units as play percentage. So if note is 0x7F, and portamento is 0x7F, it will take the whole note to slide.


NOTE about using portamento in midi sequencers.
Portamento is basically a giant pain in the ass to use. It is comprised of 3 midi controller events.
CC 5 - Time (no idea????)
CC 65 - ON/OFF (0 to 63 = Off, 64 to 127 = On)
CC 84 - Target (Is a note number)
and it only works in certain modes on certain instruments and also depending on which program
you use it can sometimes just not work????

Not supported by SEQ64 unless you just want to place the cmd in inaccurately,
which is fine because midi software doesn't do it either

]
---------------------------------------------------------------------------
[LEGATO
to smoothly transition between notes
IF and only if there is a non zero time between when one note sound ends, and
the next begins, the next note will continue the previous notes properties at
the new pitch and for the new duration.

This means the volume, and place it is playing in the sample is carried over.
This gives the illusion of a continous note.

MIDI CC 68, called layer something on/off in decomp which is awful.
Is a binary ON/OFF but in midi is 0-63 off, 64-127 ON.
May not import properly for this reason.

]
---------------------------------------------------------------------------
[SHORT NOTES
short in the sense that the notes take less space.
They use the same velocity for all the notes, which is set in a separate cmd.
They can also use a default play percentage, in order to have notes that only use one byte.
No reason to use unless you are tight for space in your midi

NO seq64 support

]
---------------------------------------------------------------------------
[RESERVED NOTES
I have no clue

No support
]
---------------------------------------------------------------------------
[SET/FREE/END LAYER
set will init all the layer data and effects.
Free layer, removes the layer from the pool, this only frees up memory
End will disable the channel from being processed. This will free all layers.

Auto added by seq64 in all forms.
]
---------------------------------------------------------------------------
[VALUE
A variable used for dynamic activity, which takes input from within the sequence.
Can be set to true if a child object is finished, using a test<obj> finished.
Can be explicitly set inside channels/sequences
Can be read from an offset using read IO

Can be used to branch, jump, and write. Most often used to dynamically set
effect values such as reverb or delay inside the SFX player.

Dyntables used in channels and layers will jump to the index indicated by the
value as long as it is not -1 (e.g. a NULL value).

Not supported natively in midi.
]
---------------------------------------------------------------------------
[VIBRATO
Has a rate, extent and delay values. Can also be set to be linear, or constant
Supported by seq64 2.0, but not 1.x.

Vibrato updates 240 times a second. It only syncs with note times at a bpm
of 120. If you use a vibrato ramp, it will not sound the same at different
tempos.

Default vibrato rate is 0x800.
On constant, rate is set to arg * 32

Rate is midi CC 76.

Default vibrato extent is 0
It is set to arg * 8 on both. Extent determines how much pitch change there is.

Extent is midi CC 77.

Vibrato changes pitch using a sine wave, going for a max of +/- 127 notes.
The extent multiplies the frequency from the default pitch table for all notes
by the extent divided by 4096.0f
So an extent of 255, at max can travel half the full pitch table, which equates
to 8 octaves.

Rate determines how fast it shifts. Rate is increased every update, and bitshifted
down by 10. So this means every update, the pitch is changed by the rate/0x400.
A rate of 0x800, means that the pitch changes by two notes worth every update.

Delay is the number of updates before vibrato starts. It is equal to the arg,
times 16.

Delay is midi CC 75.

Linear vibrato takes an additional 2 args. It starts at the first arg, and ramps
to the second arg over time duration of the third arg. Durations are the same
values as delay is, e.g. updates * 16.

Vibrato is reset per note, even with legato on.

With linear, three CC are used.

Rate Linear, start - CC 78, target - CC 79, Time - CC 80

Extent Linear, start - CC 81, target - CC 82, Time - CC 83

Linear stuff will not play properly in standard midi, but default vibrato will (hopefully
depending on the instrument xDDDDDDDD suffering).
]
---------------------------------------------------------------------------
[TEMPO
Tempo is set at 120 at a default. It has a max of 255.
Tempo affects note timings, but does not affect many other effects, such
as vibrato, but portamentos are affected.

Only set tempo is midi supported and seq64 supported.
]
---------------------------------------------------------------------------
[ALLOC POLICY
notes can be allocated into a layer, channel, sequence of the free pool.
This is set by a bitmask in the channel, or in the sequence. I don't have
any particular experience on if this matters at all, but I imagine it helps
if you're running out of memory or something.
From testing, only the free pool 8, and layer pool 1, worked.
]
---------------------------------------------------------------------------
[PAN/SUSTAIN/REVERB/RELEASE RATE/PITCH BEND/TRANPOSE

pan, pitch bend (but scaled wrong), tranpose always supported
reverb supported by seq64 2.0, but not 1.x.

Reverb is CC event 91.

Sustain is CC event 64.

Release rate is CC event 72.

Others are supported directly/more commonly.

pan determines left right mixing. Can be set per layer or channel.
Channel can set pan mix, which sets the pan scaling. 128 is for 1.0 scaling.

Sustain determines the volume held by a note after it stops playing and how
long it takes to decay to zero (maybe just vol? not clear)

Release determines how long it takes the sound to decay to 0. A smaller number
is longer.

Reverb does exactly as it describes. Scales from 0 to 255.

Pitch bends can change to a new note by looking up a frequency table change,
or by doing a raw frequency multiplier. Max for standard bend is 1 octave.
One note is then 10.58, or just 10 and 11 alternating. (Ratio of 12 notes per octave
to 127 in pitch bend per octave).
With a raw change, you have to calculate the change by doing the int/0x8000.
The max increase is one octave, but downwards it can go to zero. 0x4000 is
one octave down, 0x2000 is two etc.

Transpose exists in all objects. It acts as expected, it shifts in semi tones.
]
---------------------------------------------------------------------------
[MUTE BHV/SCALE/VOLUME
A bitflag for game options that would lower volume or mute.
Mute scale determines the lower volume, the bitflags are shown in decomp:

#define MUTE_BEHAVIOR_STOP_SCRIPT 0x80 // stop processing sequence/channel scripts
#define MUTE_BEHAVIOR_STOP_NOTES 0x40  // prevent further notes from playing
#define MUTE_BEHAVIOR_SOFTEN 0x20      // lower volume, by default to half

You can set mute behavior per channel aswell, but not the mute volume.

Sequence can be manually muted with a seq_mute cmd. If you were scripting somehow
and actually didn't want to die you could do this. Idk why.

Volume can be set relative, or hardset per channel and sequence.
You can also set a volume scale, which scales the volume
by a float. Possibly used for a macro level scaling? Kind of redundant.

Auto added by seq64 but not supported by default in midi.
]
---------------------------------------------------------------------------
[FLOW CONTROL
It all acts like value does, if you don't know asm you won't get it.
Basically just learn how it works dummy lol.
]
---------------------------------------------------------------------------