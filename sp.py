from pathlib import Path
import re
import os
import json
import audioread
from functools import partial
from dataclasses import dataclass
import struct
import subprocess
#bpm, 48 ticks per beat
gTempo = 120

#given a decomp repo path, this finds sound files needed
class FileManager():
	Bank_Map = {
		8:"mario",
		1:"1",
		0:"0",
		10:"mario_peach",
		1:"terrain",
		4:"4",
		5:"5",
		6:"6",
		7:"7",
		3:"3",
		9:"9",
		2:"water"
	}
	"""
	offset:(name,type,len)
	MarkerChunk = {
		4:('siz','>L',4),
		8:('numMarkers','>H',2),
	}
	
	Marker = {
		0:('ID','>H',2),
		4:('Position','>L',4),
		8:('LeadingLen','>B',1)
	}
	"""
	head = """sequence_start equ 0
.include "seq_macros.asm"
.n64
.create "dyncall.bin", 0\n\n"""
	def __init__(self,decomp):
		self.decomp = Path(decomp)
		if not self.decomp.exists():
			raise Exception('decomp repo does not exist')
		self.sequence = self.decomp / "sound/sequences/00_sound_player.s"
		if not self.sequence.exists():
			raise Exception('sound player does not exist')
		self.Banks = {}
		self.GetSoundBanks()
	def GetSoundBanks(self):
		dir = self.decomp / (f"sound/sound_banks/")
		for b,n in self.Bank_Map.items():
			with open(os.path.join(dir,os.listdir(dir)[b]),'r') as js:
				self.Banks[b] = Bank(json.load(js))
				self.Banks[b].name = os.listdir(dir)[b]
	def GetSample(self,bank,inst):
		return self.Banks[bank].GetSample(inst,self)
	def GetAIFFMarkers(self,sample):
		with open(sample,'rb') as smp:
			#marker chunk appears after FORM and COMM and is optional. FORM is 12 bytes long, and COMM is 26 bytes long
			smp = smp.read()
			if smp[0x26:0x2A] != b'MARK':
				return None
			else:
				Chunks = []
				Chunks.extend(struct.unpack(">LH",smp[0x2A:0x2A+6]))
				Marks = {}
				x = 0
				for c in range(Chunks[1]):
					ID,pos,pStrLen = struct.unpack(">HLB",smp[0x30+x:0x30+x+7])
					x += pStrLen+7
					Marks[ID] = pos
				return Marks
	def AsmOut(self):
		f = open("tmp.asm",'w')
		f.write(self.head)
		return f
	def TmpLoad(self):
		t = open("tmp.asm",'r')
		L = t.readlines()
		t.close()
		return L
	def SavePlayer(self,lines):
		p = open("00_sound_player.s",'w')
		[p.write(l) for l in lines]
		p.close()
	def RefreshLines(self):
		with open(self.sequence,'r') as seq:
			return seq.readlines()

#maps instruments to sound samples
class Bank():
	def __init__(self,js):
		self.js = js
	def GetInst(self,inst):
		name = self.js['instrument_list'][inst]
		return self.js['instruments'][name]
	def GetSample(self,inst,fm):
		fold = self.js['sample_bank']
		return fm.decomp / (f'sound/samples/{fold}/{self.GetInst(inst)["sound"]}.aiff')
	def GetEnvelope(self,inst):
		return self.js['envelopes'][self.GetInst(inst)['envelope']]

#for generic handling of seq asm files
class ASM():
	def print(self):
		print(self.dat)
	#generic text parse function
	def Parse(self,label,end,container,action):
		JP = 0
		if not (type(end) is list or type(end) is tuple):
			end = [end]
		st = REindex(label,self.lines)+1
		self.start = st
		for i,l in enumerate(self.lines[st:]):
			attr = SplitArgs(l)
			if any([e in l for e in end]):
				container.append((attr[0],attr[1]))
				break
			if '.ifdef' in l:
				JP += 1
				continue
			if JP and ('.else' in l or '.endif' in l):
				JP -= 1
				continue
			if JP == 0 and ('.else' in l or '.endif' in l):
				continue
			if JP>0:
				continue
			if attr[0]:
				container.append((attr[0],attr[1]))
			if action:
				action(st,i,attr)
	#get layers with the necessary associated data (bank,inst for now)
	def ParseChan(self):
		bank = 0
		inst = 0
		layers = []
		envelopes = []
		for cmd in self.dat:
			if cmd[0] == 'chan_setenvelope':
				envelopes.append(Envelope(cmd[1][0],self.fm,self.lines))
			if cmd[0] == 'chan_setbank':
				bank = cmd[1][0]
			if cmd[0] == 'chan_setinstr':
				inst = cmd[1][0]
			if cmd[0] == 'chan_setlayer':
				layers.append(Layer(bank,inst,cmd[1][1],self.fm,self.lines))
		self.layers = layers
		self.envelopes = envelopes
	def Write(self,file,dat,label,replacements):
		q = self.VirtualWrite([],dat,label,replacements)
		[file.write(l) for l in q]
	#turn data into list properly text formatted
	def VirtualWrite(self,L,dat,label,replacements):
		L.append(label)
		for d in dat:
			#some stuff isn't comma separated for some reason so I guarantee it here
			for j,c in enumerate(d[1][:-1]):
				if ',' not in c:
					d[1][j] += ','
			line = "{} {}\n".format(d[0],(' '.join(d[1])))
			for r in replacements:
				line = line.replace(*r)
			L.append(line)
		L.append("\n")
		return L
	def Replace(self,dat,label,replacements,start,ins):
		q = self.VirtualWrite([],dat,label,replacements)
		for j,l in enumerate(q):
			if ins:
				self.lines.insert(start+j-1,l)
			else:
				self.lines[start+j-1] = l
		return start+j

#holds envelope ASDR values
class Envelope(ASM):
	def __init__(self,env,fm,lines):
		self.env = env+':\n'
		self.lines = lines
		self.fm = fm
		self.dat = []
		self.Parse(self.env,('envelope_disable','envelope_hang','envelope_goto','envelope_restart'),self.dat,None)

#layer, where notes get played
class Layer(ASM):
	def __init__(self,bank,inst,layer,fm,lines):
		self.bank = int(bank)
		self.inst = int(inst)
		self.layer = layer+':\n'
		self.lines = lines
		self.fm = fm
		self.transpose = 0
		self.dat = []
		self.Parse(self.layer,'layer_end',self.dat,None)

#the actual sfx, pointed to by a channel, contains layers that hold notes
#this is essentially a channel, minus the main loop stuff and things
class Sound(ASM):
	def __init__(self,ID,num,lines,fm):
		self.ID = ID
		self.num = num
		self.dat = []
		self.layers = None
		self.fm = fm
		self.lines = lines
	def GetSoundData(self):
		ID = self.ID+":\n"
		#deal with potential jumps etc with action, but not now
		self.Parse(ID,('chan_end','chan_external_pop'),self.dat,None)
		self.ParseChan()
	def Output(self,bank,ID):
		#output the sound to a tmp .asm file
		if not self.dat:
			self.GetSoundData()
		f = self.fm.AsmOut()
		f.write("\n;Bank {} ID {} made with sp.py, edit and save to put changes to file\n;Beware of shared lables (envelopes and layers more than sounds)\n".format(bank,ID))
		self.Write(f,self.dat,self.ID+":\n",[("chan_end","chan_external_pop")])
		if hasattr(self,"layers"):
			[self.Write(f,l.dat,l.layer,[]) for l in self.layers]
		if hasattr(self,"envelopes"):
			[self.Write(f,e.dat,e.env,[]) for e in self.envelopes]
		f.write("\n.close")
		f.close()
		subprocess.call(['armips','tmp.asm'])
	def Save(self,bank,ID):
		L = self.fm.TmpLoad()
		New = Sound(self.ID,self.num,L,self.fm)
		New.GetSoundData()
		ins = 0
		end = self.Replace(New.dat,New.ID+":\n",[("chan_external_pop","chan_end")],self.start,ins)
		if hasattr(New,"layers"):
			for l in New.layers:
				for z in self.layers:
					if l.layer == z.layer:
						start = z.start
						break
				else:
					start = end+1
					ins = 1
				end = self.Replace(l.dat,l.layer,[],start,ins)
				ins = 0
		if hasattr(New,"envelopes"):
			for l in New.envelopes:
				for z in self.envelopes:
					if l.env == z.env:
						start = z.start
						break
				else:
					start = end+1
					ins = 1
				self.Replace(l.dat,l.env,[],start,ins)
		self.fm.SavePlayer(self.lines)
		self.lines = self.fm.RefreshLines()

#second level, there are up to 16 of these, pointed to by seq header
class Channel(ASM):
	def __init__(self,ID,num,lines,fm):
		self.ID = ID
		self.num = num
		self.sounds = {}
		self.dat = []
		self.layers = None
		self.fm = fm
		self.lines = lines
	def GetChannelData(self):
		n = self.ID+":\n"
		#deal with potential jumps etc with action, but not now
		self.Parse(n,('chan_end','chan_external_pop',":"),self.dat,None)
		self.ParseChan()
		self.GetDynSounds()
	def GetDynSounds(self):
		for i in self.dat:
			if 'chan_setdyntable' in i[0]:
				dyn = i[1]
				break
		else:
			return
		x = 0
		JP = 0
		if dyn:
			dyn = dyn[0]+":\n"
			for l in self.lines[REindex(dyn,self.lines)+1:]:
				if ":" in l:
					break
				if '.ifdef' in l:
					JP += 1
					continue
				if JP and ('.else' in l or '.endif' in l):
					JP -= 1
					continue
				if 'sound_ref' in l and JP == 0:
					self.sounds[x] = Sound(SplitArgs(l)[1][0],x,self.lines,self.fm)
					x += 1

#highest level, the entire file
class Sequence(ASM):
	def __init__(self,lines,fm):
		self.lines = lines
		self.fm = fm
		self.channels = {}
		self.GetChannels()
	def GetChannels(self):
		for l in self.lines:
			if 'seq_startchannel' in l:
				b = l.split(',')
				self.channels[int(b[0].split(' ')[1])] = Channel(b[1].strip(),int(b[0].split(' ')[1]),self.lines,self.fm)
		for c in self.channels.values():
			c.GetChannelData()

#for high level outputs
class Output():
	def __init__(self,fm,seq):
		self.seq = seq
		self.fm = fm
	def Sound(self,bank,ID):
		snd = self.seq.channels[bank].sounds[ID]
		snd.Output(bank,ID)
	def Channel(self,ID):
		pass
	def Seq(self):
		pass
	def SaveSnd(self,bank,ID):
		snd = self.seq.channels[bank].sounds[ID]
		snd.Save(bank,ID)
	def Assemble(self):
		subprocess.call(['armips','tmp.asm'])

#search a list with regex
def REindex(exp,L):
	regex = re.compile(exp)
	for i, item in enumerate(L):
		if re.search(regex, item):
			return i

#split a macro into macro and its args
def SplitArgs(l):
	l = l.replace('\n','')
	l = l.strip()
	l = l.split(' ')
	cmd = l[0]
	args = l[1:]
	return (cmd,args)

def GetSequence(FM):
	with open(FM.sequence,'r') as seq:
		return Sequence(seq.readlines(),FM)

def InitFM(path):
	return FileManager(path)

def GetAll(path):
	fm = InitFM(path)
	seq = GetSequence(fm)
	return fm,seq

#get a sound from game bank/id
def OutputSound(sequence,fm,bank,ID):
	snd = sequence.channels[bank].sounds[ID]
	snd.Output()
	return snd

def Duty(val4):
	return (256-val4)/256

def RConv(note):
	return 2**((note-39)/12)

def Vel(a):
	return a/127

def Note0(args,dur,transpose):
	s = (args[1]/(96*dur))*Duty(args[3])
	print((RConv(args[0]+transpose),s*RConv(args[0]+transpose),args[2]/0x7F),dur,args)
	return [(RConv(args[0]+transpose),s*RConv(args[0]+transpose),args[2]/0x7F),(args[1]/96)]

def Note1(args,dur,transpose):
	s = args[1]/(96*dur)
	print((RConv(args[0]+transpose),s*RConv(args[0]+transpose),args[2]/0x7F),dur,args)
	return [(RConv(args[0]+transpose),s*RConv(args[0]+transpose),args[2]/0x7F),(args[1]/96)]

#test code for playing samples
def smp():
	s = "C:/hackport/sm64ex-alo/sound/samples/sfx_5/0D.aiff"
	dur = audioread.audio_open(s).duration/96 #seconds
	ps.sample(s,finish=dur*Duty(20)*13,rate=RConv(36))
	print('sample ', s,-3,dur*Duty(20)*13,RConv(36))

#quick for debug
def a():
	out = Output(*GetAll('C:\hackport\sm64ex-alo'))
	return out

